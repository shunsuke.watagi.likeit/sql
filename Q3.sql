﻿create database test2 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

use test2;
create table item_category(
category_id int primary key auto_increment,
category_name varchar(256) not null);


insert into item_category(category_id,category_name)values(1,'家具');
insert into item_category(category_id,category_name)values(2,'食品');
insert into item_category(category_id,category_name)values(3,'本');


